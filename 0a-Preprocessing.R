##################################################
##  Project: Preprocessing of Video Ratings
##  Date: 14.11.20
##  Author: Laura Israel 
##  Reviewed and refactored by: Felix Schönbrodt
##################################################

# Load library
library(reshape2)
library(dplyr)
library(tidyr)


## Load data
dat <- read.csv("raw_data/video_ratings.csv")
dur_dat <- read.csv("raw_data/assignment_duration.csv")
dat_info <- read.csv("raw_data/assignment_information.csv")
vid_dat <- read.csv("raw_data/video_step_duration.csv")


## Calculate duration of HIT and video duration
dur_dat$dur_in_sec <- as.numeric(difftime(dur_dat$end_time,dur_dat$start_time, units = "secs"))
vid_dat$vid_in_sec <- as.numeric(difftime(vid_dat$end_time,vid_dat$start_time, units = "secs"))


## Build variables to merge data sets later on

# Build merge variable (worker_id + assignment id) in full data set
dat$merge_var <- gsub(" ", "", apply(dat, 1, function (x) { paste(as.character(x["worker_id"]), as.character(x["id"]), sep = "_")}))

# Build merge variable (worker_id + assignment id) for assignment duration
dur_dat$merge_var <- gsub(" ", "", apply(dur_dat, 1, function (x) { paste(as.character(x["worker_id"]), as.character(x["assignment_id"]), sep = "_")}))
dur_dat <- dur_dat[,c("dur_in_sec", "merge_var")]

# Build merge variable (worker_id + assignment id) for assignment info
dat_info$merge_var <- gsub(" ", "", apply(dat_info, 1, function (x) { paste(as.character(x["worker_id"]), as.character(x["id"]), sep = "_")}))
dat_info <- dat_info[,c("quality", "merge_var")]


## Build variable that encodes the duration of all videos in a HIT
# (one HIT contained 4 videos)

# Calculate sum of video durations for each hit
vid_dat$merge_var <- gsub(" ", "", apply(vid_dat, 1, function (x) { paste(as.character(x["worker_id"]), paste(as.character(x["assignment_id"]), sep = "_"), sep = "_")}))

# Add new summed variable to duration data 
dur_dat$vid_dur_sum <- NA

# Sum up video durations for each hit
for (i in dur_dat$merge_var) {
  dur_dat[dur_dat$merge_var == i, "vid_dur_sum"] <- sum(vid_dat[vid_dat$merge_var == i, "vid_in_sec"])
}


## Rename video_code (prefixes A, B, and C are irrelevant; the video code is unique)
dat$video_code <- gsub("C_", "VID_", dat$video_code)
dat$video_code <- gsub("B_", "VID_", dat$video_code)
dat$video_code <- gsub("A_", "VID_", dat$video_code)


## Recode questions to item names

# Create new var from question and subquestion
dat$question <- paste(dat$question_id, dat$sub_question_id, sep="_")
dat$question <- gsub("-", "_", dat$question, fixed=TRUE)
dat$question <- gsub("_NA", "", dat$question, fixed=TRUE)
dat <- dat %>% select(-question_id, -sub_question_id)

# Rename all question vars with proper item names
dat$question[dat$question == "emotional_experience1"] <- "no_emo_no_match" # -1 = no match; -2 = no emotion
dat$question[dat$question == "emotional_experience1_0"] <- "emo_1"
dat$question[dat$question == "emotional_experience1_1"] <- "emo_2"
dat$question[dat$question == "emotional_experience2_0"] <- "reduce_intensity"
dat$question[dat$question == "emotional_experience3_0"] <- "PANAVA_dissatisfied"
dat$question[dat$question == "emotional_experience3_1"] <- "PANAVA_weak"
dat$question[dat$question == "emotional_experience3_2"] <- "PANAVA_relaxed"
dat$question[dat$question == "emotional_experience3_3"] <- "PANAVA_wide_awake"
dat$question[dat$question == "emotional_experience3_4"] <- "PANAVA_angry"
dat$question[dat$question == "emotional_experience3_5"] <- "PANAVA_happy"
dat$question[dat$question == "emotional_experience3_6"] <- "PANAVA_motivated"
dat$question[dat$question == "emotional_experience3_7"] <- "PANAVA_nervous"
dat$question[dat$question == "emotional_experience3_8"] <- "PANAVA_bored"
dat$question[dat$question == "emotional_experience3_9"] <- "PANAVA_carefree"
dat$question[dat$question == "pleasant_0"] <- "APP_pleasantness"
dat$question[dat$question == "characteristics_0"] <- "APP_suddenness"
dat$question[dat$question == "characteristics_1"] <- "APP_predictability"
dat$question[dat$question == "characteristics_2"] <- "APP_familiarity"
dat$question[dat$question == "characteristics_3"] <- "APP_goal_importance"
dat$question[dat$question == "characteristics_4"] <- "APP_internal_standards"
dat$question[dat$question == "characteristics_5"] <- "APP_external_standards"
dat$question[dat$question == "predictability_0"] <- "APP_expectation"
dat$question[dat$question == "predictability_1"] <- "APP_probability"
dat$question[dat$question == "causation_0"] <- "APP_cause_agent_nature"
dat$question[dat$question == "causation_1"] <- "APP_cause_agent_protagonist"
dat$question[dat$question == "causation_2"] <- "APP_cause_agent_other_person"
dat$question[dat$question == "causation_3"] <- "APP_cause_motive"
dat$question[dat$question == "reactions_0"] <- "APP_urgency"
dat$question[dat$question == "reactions_1"] <- "APP_adjustment"
dat$question[dat$question == "reactions_2"] <- "APP_control"
dat$question[dat$question == "consequences_0"] <- "APP_conduciveness"
dat$question[dat$question == "protagonist_perspective_0"] <- "identification_1"
dat$question[dat$question == "protagonist_perspective_1"] <- "identification_2"
dat$question[dat$question == "protagonist_perspective_2"] <- "APP_goal_importance_protagonist"
dat$question[dat$question == "protagonist_perspective_3"] <- "APP_urgency_protagonist"
dat$question[dat$question == "protagonist_perspective_4"] <- "APP_adjustment_protagonist"
dat$question[dat$question == "protagonist_perspective2_0"] <- "APP_conduciveness_protagonist"
dat$question[dat$question == "speak_day"] <- "CHECK_speak_day"
dat$question[dat$question == "ocean_air_balloon"] <- "CHECK_ocean_air_balloon"
dat$question[dat$question == "music_never"] <- "CHECK_music_never"
dat$question[dat$question == "speak_day"] <- "CHECK_speak_day"
dat$question[dat$question == "sleep_per_night"] <- "CHECK_sleep_per_night"
dat$question[dat$question == "middle_name"] <- "CHECK_middle_name"

## Delete non-unique answers (there are a few duplicates in the data set)
dat <- dat[-as.numeric(row.names(dat[duplicated(dat[, c("merge_var", "video_code", "question")], fromLast = TRUE), ])),]

## Reshape to wide format
## !!! As some videos were terminated (hence the participants did not answer all items), spreading the data leads to some missings in the data 
dat.wide <- pivot_wider(dat %>% select(-intensity), id_cols=c(worker_id, id, video_code, merge_var), names_from=question, values_from=answer, names_sort=TRUE)

## Build wide format for intensity ratings
dat.wide2 <- pivot_wider(dat, 
                         id_cols=c(worker_id, id, video_code, merge_var), 
                         names_from=question, 
                         values_from=intensity, names_sort=TRUE) %>% 
    select(intensity_1=emo_1, intensity_2=emo_2)


## Bind to wide dfs together
dat.wide3 <- cbind(dat.wide, dat.wide2)

## Add No emotion and NO match category to emo1: These numeric codes later are converted into proper factors
dat.wide3$emo_1[dat.wide3$no_emo_no_match == -2] <- 99
dat.wide3$emo_1[dat.wide3$no_emo_no_match == -1] <- 1099

## Recode missings (-1) to NA
dat.wide3[dat.wide3 == -1] <- NA

## Merge with duration data 
merged <- left_join(dat.wide3, dur_dat, by = "merge_var")
merged2 <- left_join(merged, dat_info, by = "merge_var")

## Add variable that codes assignment duration without video duration
merged2$dur_wo_video <- merged2$dur_in_sec - merged2$vid_dur_sum



## Data cleaning: Remove some invalid workers or HITs
##------------------------------------------------------------------------------

# find double assignments: 26 videos have been assigned multiple times to the same person
merged2 %>% group_by(worker_id, video_code) %>% summarise(n=n()) %>% pull("n") %>% table()
merged3 <- merged2 %>% group_by(worker_id, video_code) %>% slice(1)

## Save data
saveRDS(merged3, "processed_data/video_ratings.RDS")


