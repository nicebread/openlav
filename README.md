# Accompanying source code for 'The OpenLAV Video Database for Affect Induction - Analyzing the Uniformity of Video Stimuli Effects'

This is the accompanying source code for:

TODO: OSF Preprint citation



* `/codebooks` contains all codebooks and data set documentation. `codebook_software.json` is the source of the crowdsourcing program with all item wordings. See README file in that folder for more details on the codebooks.
* `/raw_data` stores all primary data sets necessary to reproduce all results. See README.txt in that folder for a description of the files.
* `/processed_data` stores intermediate, cached data or result files. All files in this folder can safely be deleted, as they are recreated from the scripts.
* `/export` stores the final and fully documented data files. For reuse of the data, we strongly recommend to use these files. These are the files stored on the psychArchives repository.
* `/plots` stores intermediate plots that are used in the paper.

# Reproducing the paper

To fully reproduce the paper, do these steps in the given order:

* **Preprocessing**: Execute all R scripts (from `0a-Preprocessing.R` to `0c_Data_Quality_Metrics.R`) in the provided order. These scripts take the raw data files in `/raw_data` and preprocess them. Resulting processed data files are saved in `/processed_data`. No data exclusions are done up to this point. At the end, two data files are saved: `full_data.csv` (which contains all available data) and `valid_data.csv` (which is contains all data that pass our inclusion criteria). The latter is then used for all subsequent steps.
* **Analyses**: Execute all R scripts (from `1_Valence_Arousal_Factor_Scores.R` to `3_Personality_Importance.R`) in the provided order. Again, intermediate results are saved in `/processed_data`.
* **Paper compilation**: Compile `Paper_OpenLAV.Rmd`. This computable manuscript does *some* of the actual computations; some computations, however, are done in the preprocessing and analysis scripts and are loaded from the cached results stored in `/processed_data`.
* **Data set export**: Run `Data_Set_Export.R`. This script must be executed last, as it accesses a data set created in the Rmd file.

The script `00-Make.R` does all of these steps sequentially. Depending on your processor speed, computation needs around 20 minutes.

After all processing, the final computed paper is in `Paper_OpenLAV.pdf`.



TODOs:

- OSF Preprint citation in this README.
