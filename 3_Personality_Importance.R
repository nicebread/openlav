##################################################
##  Project: Personality Feature Importance in RF Model
##  Date: 28.10.20
##  Author: Laura Israel 
##  Reviewed by: Felix Schönbrodt
##################################################

## Load data set with factor scores and clear emotion label
dat <- read.csv("processed_data/valid_data.csv")

library(dplyr)
library(mlr)
library(ranger)
library(party)
library(moreparty)
library(measures)
library(ggplot2)
library(gridExtra)

## Set seed
set.seed(1234)

## Select data
rf_dat <- dat[, c("worker_id", "video_code", "ar_fac_scores", "val_fac_scores", "prim_emo", "N_s", "E_s", "O_s", "A_s", "C_s", "ACH_s", "AFF_s", "POW_s", "INT_s", "FEAR_s")]
rf_dat <- rf_dat[complete.cases(rf_dat), ] ## remove 4 missings

## Build video code variable
video_code <- rf_dat$video_code

## Make regression task arousal
ar_task <- makeRegrTask(
  id = "Arousal Prediction",
  data = rf_dat %>% dplyr::select(contains("_s")),
  target = "ar_fac_scores",
  blocking = as.factor(rf_dat$worker_id)
)

## Make regression task valence
val_task <- makeRegrTask(
  id = "Valence Prediction",
  data = rf_dat %>% dplyr::select(contains("_s")),
  target = "val_fac_scores",
  blocking = as.factor(rf_dat$worker_id)
)

## Make classification task emotion labels
emo_task <- makeClassifTask(
  id = "Emo Prediction",
  data = rf_dat %>% dplyr::select(contains("_s"), contains("emo"), -ar_fac_scores, -val_fac_scores),
  target = "prim_emo",
  blocking = as.factor(rf_dat$worker_id)
)



## ----------------------------------------------------------------
## Feature Importance (Main Effect) for all Personality Features
## ----------------------------------------------------------------

# training_frac = fraction used for training set; rest goes into the test set
calculate_feature_imp <- function(task_x, k=100, training_frac = 0.8, num.trees=500, max.depth=5) {
  
  ## Bind task object
  task <- task_x
  
  ## Get task data
  taskdat <- getTaskData(task)
  
  ## Get variable names
  vars  <- getTaskFeatureNames(task)
  
  ## Get target name
  target <- getTaskTargetNames(task)
  
  ## Get task type
  task_type = task$type
  
  ## Output matrix
  output_mat <- matrix(nrow = k, ncol = length(vars))
  colnames(output_mat) <- vars
  
  ## Vector with baseline values for classification task (largest class in data set)
  baseline_vec <- NULL
  
  ## 100 repeated hold outs
  for (cv in 1:k) {
    cat(paste0(cv, " - "))
    ## Sample data blocked by subject
    
    ## Get subject vector without excluded observation for present appraisal
    unique_ids <- unique(rf_dat$worker_id)
    
    ## Sample 70% of the subjects included in the present data set
    subject_samples <- sample(unique_ids, round(training_frac*length(unique_ids), 0), replace = FALSE)
    
    ## As train indices, take only observations of the training samples ids
    train_indices <- rf_dat$worker_id %in% subject_samples
    
    ## Residualize training set
    train_dat <- taskdat[train_indices,]
    train_dat_videos <- video_code[train_indices]
    
    for (vid in unique(train_dat_videos)) {
      train_dat[train_dat_videos == vid, colnames(train_dat) != target]  <-  apply(train_dat[train_dat_videos == vid, colnames(train_dat) != target], 2, function(x) {x <- x - mean(x, na.rm = TRUE)})
    }
    
    if (task_type == "classif") {
      baseline_vec <- c(baseline_vec, max(table(train_dat$prim_emo))/nrow(train_dat))
    }
    
    ## Residualize test set: Subtract the mean value of each video
    test_dat <- taskdat[!train_indices,]
    test_dat_videos <- video_code[!train_indices]
    
    for (vid2 in unique(test_dat_videos)) {
      test_dat[test_dat_videos == vid2, colnames(test_dat) != target]  <-  apply( test_dat[test_dat_videos == vid2, colnames(test_dat) != target], 2, function(x) {x <- x - mean(x, na.rm = TRUE)})
    }
    
    
    ## For each variable
    for (i in vars) {
      ## Fit ranger on sampled data with only selected variable i as predictors
      output.ranger  <-
        ranger(
          paste0(target, " ~ .") ,
          data = train_dat[, c(target, i)],
          num.trees = num.trees,
          max.depth = max.depth
        )
      
      ## Predict with hold out data
      predictions <- predict(output.ranger, data = test_dat[, c(target, i)])$predictions
      
      ## Calculate either R^2 for regression or accuarcy for classification
      if (task_type == "regr") {
        perf <- RSQ(test_dat[, target], predictions)
      }
      if (task_type == "classif") {
        perf <- ACC(test_dat[, target], predictions)
      }
      
      ## Save R^2 to matrix
      output_mat[cv, i] <- perf
    }  # of  for (i in vars)
  } # of for (cv in 1:100)
  
  perf_M <- apply(output_mat, 2, mean)
  perf_VAR <- apply(output_mat, 2, var)
  
  # in classification tasks: compare accuracy performance not against zero, but against a featureless learner (i.e., the baseline vec with the largest class fraction)
  # this is used for the hypothesis test
  if (task_type == "classif") {
    perf_M_hyp <- perf_M - mean(baseline_vec)
  } else {
    perf_M_hyp <- perf_M
  }
  
  # correction of variance proposed by Nadeau & Bengio (2003)
  n1 <- nrow(taskdat)*training_frac
  n2 <- nrow(taskdat)*(1-training_frac)
  perf_VAR_corrected <- (1/k + (n2/n1))*perf_VAR
  
  t_value <- perf_M_hyp / sqrt(perf_VAR_corrected)
  df <- k-1
  p_value <- pt(t_value, df=df, lower.tail=FALSE)
  
  ## Return the importances aggregated over all 100 iterations
  res <- data.frame(target=target, feature=names(p_value), perf_M, p_value)
  rownames(res) <- NULL
  return(res)
}  # of calculate_feature_imp()

## ------------------------------------------------------------
## Tuning of RF hyperparameters (num.trees & max.depth): Do it in the ar_task (for continuous outcomes) and in the emotion label classification
## Commented out for production
## ------------------------------------------------------------

# tuning_grid_cont <- expand.grid(num.trees=c(500, 1000), max.depth=c(2, 3, 4, 5, 6))
# tuning_res_cont <- data.frame()
# for (i in 1:nrow(tuning_grid_cont)) {
#   print(tuning_grid_cont[i, ])
#   set.seed(1234) # use same seed in each tuning condition, for same splits.
#   ar_imp_tune <- calculate_feature_imp(task_x=ar_task, k=50, training_frac = 0.8, num.trees=tuning_grid_cont$num.trees[i], max.depth=tuning_grid_cont$max.depth[i])
#   tuning_res_cont <- rbind(tuning_res_cont, cbind(ar_imp_tune, tuning_grid_cont[i, ]))
#   print(tuning_res_cont)
# }
# 
# tuning_res_cont %>% filter(feature=="val_fac_scores")
# 
# 
# tuning_grid_cat <- expand.grid(num.trees=c(500, 1000), max.depth=c(2, 3, 4, 5, 6))
# tuning_res_cat <- data.frame()
# for (i in 1:nrow(tuning_grid_cat)) {
#   print(tuning_grid_cat[i, ])
#   set.seed(1234) # use same seed in each tuning condition, for same splits.
#   ar_imp_tune <- calculate_feature_imp(task_x=emo_task, k=50, training_frac = 0.8, num.trees=tuning_grid_cat$num.trees[i], max.depth=tuning_grid_cat$max.depth[i])
#   tuning_res_cat <- rbind(tuning_res_cat, cbind( ar_imp_tune, tuning_grid_cat[i, ] ))
#   print(tuning_res_cat)
# }
# 
# tuning_res_cat %>% group_by(num.trees, max.depth) %>% summarise(pref=mean(perf_M))


## Calculate Feature Importance for all three tasks, using the best hyperparameter settings (doesn't really make a difference)
ar_imp <- calculate_feature_imp(task_x=ar_task, k=100, training_frac = 0.8, num.trees=500, max.depth=2)
val_imp <- calculate_feature_imp(task_x=val_task, k=100, training_frac = 0.8, num.trees=500, max.depth=2)
emo_imp <- calculate_feature_imp(task_x=emo_task, k=100, training_frac = 0.8, num.trees=500, max.depth=2)

# collect p-values from trait scales (for reporting the smallest p-values in manuscript)
arval_p_values <- c(ar_imp$p_value[-1], val_imp$p_value[-1])
emo_p_values <- c(emo_imp$p_value)

## Build results matrix
mat <- rbind(ar_imp, val_imp, emo_imp)

## Save results to cache
saveRDS(mat, "processed_data/imp.RDS")
saveRDS(arval_p_values, "processed_data/arval_p_values.RDS")
saveRDS(emo_p_values, "processed_data/emo_p_values.RDS")



## ------------------------------------------------------------
## Full Performance: R^2 of Random Forest with all Features
## ------------------------------------------------------------

calculate_full_perf <- function(task_x, k=100, training_frac = 0.8, num.trees=500, max.depth=2) {
  ## Bind task object
  task <- task_x
  
  ## Get task data
  dat <- getTaskData(task)
  
  ## Get variable names
  vars  <- getTaskFeatureNames(task)
  
  ## Get target name
  target <- getTaskTargetNames(task)
  
  ## Get task type
  task_type = task$type
  
  ## Output matrix
  output_mat <- matrix(nrow = k, ncol = 1)
  
  ## 100 repeated hold outs
  for (cv in 1:k) {
    
    cat(paste0(cv, " - "))
    ## Sample data blocked by subject
    
    ## Get subject vector without excluded observation for present appraisal
    unique_ids <- unique(rf_dat$worker_id)
    
    ## Sample 70% of the subjects inlcuded in the present data set
    id_training_sample <-
      sample(unique_ids, round(training_frac * length(unique_ids), 0), replace = FALSE)
    
    ## As train indices, take only observations of the samples subs
    train_indices <- rf_dat$worker_id %in% id_training_sample
    
    ## Residualize training set
    train_dat <- dat[train_indices, ]
    train_dat_videos <- video_code[train_indices]
    for (vid in unique(train_dat_videos)) {
      train_dat[train_dat_videos == vid, colnames(train_dat) != target]  <-
        apply(train_dat[train_dat_videos == vid, colnames(train_dat) != target], 2, function(x) {
          x <- x - mean(x, na.rm = TRUE)
        })
    }
    
    ## Residualize test set
    test_dat <- dat[!train_indices, ]
    test_dat_videos <- video_code[!train_indices]
    for (vid2 in unique(test_dat_videos)) {
      test_dat[test_dat_videos == vid2, colnames(test_dat) != target]  <-
        apply(test_dat[test_dat_videos == vid2, colnames(test_dat) != target], 2, function(x) {
          x <- x - mean(x, na.rm = TRUE)
        })
    }
    
    
    ## Fit ranger on sampled data with only selected vars as predictors
    output.ranger  <-
      ranger(
        paste0(target, " ~ .") ,
        data = train_dat,
        num.trees = num.trees,
        max.depth = max.depth
      )
    
    ## Predict with hold out data
    predictions <-
      predict(output.ranger, data = test_dat)$predictions
    
    ## Calculate either R^2 for regression or accuracy for classification
    if (task_type == "regr") {
      perf <- RSQ(test_dat[, target], predictions)
    }
    if (task_type == "classif") {
      perf <- ACC(test_dat[, target], predictions)
    }
    
    
    ## Save R^2 to matrix
    output_mat[cv, 1] <- perf
    
  } # of (for cv)
  
  ## Return the importances aggregated over all 100 iterations
  return(round(mean(output_mat), 4))
} # of calculate_full_perf()


## Calculate performance for all three tasks
full_ar <- calculate_full_perf(ar_task, k=100, training_frac = 0.8, num.trees=500, max.depth=2)
full_val <- calculate_full_perf(val_task, k=100, training_frac = 0.8, num.trees=500, max.depth=2)
full_emo <- calculate_full_perf(emo_task, k=100, training_frac = 0.8, num.trees=500, max.depth=2)


## Save performance to cache
saveRDS(full_ar, "processed_data/full_ar.RDS")
saveRDS(full_val, "processed_data/full_val.RDS")
saveRDS(full_emo, "processed_data/full_emo.RDS")
