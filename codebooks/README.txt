Files in this folder:

Data set with ratings:
======================
* full_data.csv: Full data set of 13,264 video ratings (before data exclusions)
* full_data_codebook.xlsx: Codebook for the full_data.csv data set
* codebook_software.json: The original wording of the questions in the survey. Some of the items have other identifiers than the corresponding items in the full_data.csv data set; see full_data_codebook.xlsx for a mapping of identifiers.

Data set with video descriptives:
=================================
* video_data.csv: Descriptives for 188 videos (source, length, average valence & arousal ratings, entropy, etc.)
* video_data_codebook.xlsx: Codebook for the video_data.csv data set